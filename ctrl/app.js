var request = require('request').defaults({jar: true})
var querystring = require('querystring')
var fs = require('fs')
var download = require('download-pdf')


module.exports = {
  descargarImagenSial: descargarImagenSial
}

function descargarImagenSial(credito) {
  return new Promise(function(resolve, reject) {
    console.log(credito,'<---------------RECIBIMOS');
    // URL DE LOGIN
    var uri = "https://infonavit.lexaudit.com.mx/"

    // CREDENCIALES
    var form = {
      correo: 'hnava@gabssa.com.mx',
      contrasena: 'Gabssa10@'
    };

    // ENVIAR TIPO DE DATO
    var formData = querystring.stringify(form);
    var contentLength = formData.length;

    // HACER PETICION
    request({
      headers: {
        'Content-Length': contentLength,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      uri: uri,
      body: formData,
      method: 'POST'
    }, function (err, res, body) {
      var uri = `https://infonavit.lexaudit.com.mx/Dictamen_de_vivienda/list/juicio/${credito}`

      // IR POR IDENTIFICADOR UNICO
      request.get({uri: uri}, function(err, result, body){
        if (err) {
          resolve({err: true, description: err})
        } else {
          try {
            // console.log(body,'<--------BODY');
            var num = body.split('Dictamen_de_vivienda/record/')
            num = num[1].split('","_blank",')

            // IR POR TOKEN DE IMAGEN
            var uri = 'https://infonavit.lexaudit.com.mx/Dictamen_de_vivienda/record/' + num[0]
            request.get({ uri: uri }, function(err, result, body){
              if (err) {
                resolve({ err: true, description: err })
              } else {
                try {
                  var img = body.split("https://0aca28193dc89d2efdc5-2bccad991762808fdfbd79d314702ab5.ssl.cf2.rackcdn.com/")
                  img = img[1].split("'></a></div>")

                  // DESCARGAR IMAGEN
                  var uri_img = "https://0aca28193dc89d2efdc5-2bccad991762808fdfbd79d314702ab5.ssl.cf2.rackcdn.com/" + img[0]
                  var r = request(uri_img);

                  console.log(uri_img,'<-------------');
                  r.on('response',  function (res) {
                    // try {
                      res.pipe(fs.createWriteStream(`fotos/${credito}`));
                      resolve({ err: false })
                    // } catch (e) {
                    //   var options = {
                    //     directory: "./fotos",
                    //     filename: `${credito}.pdf`
                    //   }
                    //
                    //   download(uri_img, options, function(err){
                    //     if (err) throw err
                    //     resolve({ err: false })
                    //   })
                    // }
                  });
                } catch (e) {
                  resolve({ err: 1, credito: credito })
                }
              }
            })
          } catch (e) {
            resolve({ err: 1, credito: credito })
          }
        }
      })
    });
  });
}
